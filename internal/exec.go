package internal

import (
	"fmt"
	"os/exec"
)

func InstallPackage(pkgNameFullPath string) {
	fmt.Println("instalation of ", pkgNameFullPath)
	goGet := exec.Command("go", "get", "-u", pkgNameFullPath)

	ch := make(chan error)
	printProgress := true
	go func() {
		ch <- goGet.Run()
		printProgress = false
	}()
	go func() {

		counter := 0
		for {
			if !printProgress {
				break
			}
			if counter == 50 {
				counter = 0
			}
			renderString := ""
			for i := 0; i < counter; i++ {
				renderString += "="
			}
			fmt.Print("|" + renderString + ">\n \033[1A\033[K\r")
			counter++

		}
	}()
	<-ch
}
