package internal

import (
	"errors"
)

func ArgumentsFormatter(args []string, COMMANDS_ARRAY []string) ([]validCommand, error) {
	baseHelpString := `
gpm is a tool for managing the instalation for go packeges

Usage: 
	gpm <command> [value]

The commands are:

	i  <install>                  install first finded package, if multiple packages provided will install all
	id <install deep>             serch n packages and let you choose with to install
	mx <use with install deep>    max length list for serch results
`
	if len(args) == 0 {
		return nil, errors.New(baseHelpString)
	}
	command, err := parser(args, COMMANDS_ARRAY, baseHelpString)
	if err != nil {
		return nil, err
	}
	return command, err
}

type validCommand struct {
	Commad string
	Value  string
}

func parser(args []string, COMMANDS_ARRAY []string, baseHelpString string) ([]validCommand, error) {
	var validCommandsArray []validCommand
	for i, token := range args {
		for _, command_main := range COMMANDS_ARRAY {
			if token == command_main {

				if i+1 == len(args) {
					break
				}
				selectedValue := args[i+1]

				for _, command := range COMMANDS_ARRAY {
					if selectedValue == command {
						return nil, errors.New("can not run the command whithout values\n" + baseHelpString)
					}
				}

				takenIndex := i + 1
				argsLength := len(args) - 1
				if takenIndex < argsLength {
				MainLoop:
					for i := takenIndex + 1; i < len(args); i++ {
						for _, command := range COMMANDS_ARRAY {
							// if i < len(args) {
							if args[i] == command {
								break MainLoop
							}
							// }
						}
						selectedValue += "/" + args[i]
					}
				}

				c := validCommand{command_main, selectedValue}
				validCommandsArray = append(validCommandsArray, c)

			}
		}

	}

	if len(validCommandsArray) == 0 {
		return nil, errors.New("no valid command found\n" + baseHelpString)

	}
	return validCommandsArray, nil
}
